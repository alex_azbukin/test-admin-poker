# test-admin

## Project setup
```
npm install -g @vue/cli
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Run server for development
```
npm run backend
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
