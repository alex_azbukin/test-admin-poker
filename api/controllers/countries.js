const express = require('express');
const router = express.Router();
const CountryModel = require('../models/country');

router.get('/', (req, res) => {
    let response = CountryModel.getAll();
    response.then((countries) => {
        res.json(countries);
    }, (err) => {
        res.json(err);
    });
});

router.get('/prioritized', (req, res) => {
    let response = CountryModel.getPrioritized();
    response.then((countries) => {
        res.json(countries);
    }, (err) => {
        res.json(err);
    });
});

module.exports = router;