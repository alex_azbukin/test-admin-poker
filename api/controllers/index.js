const express = require('express');
const app = express();
const playersControllers = require('./players');
const countriesControllers = require('./countries');

app.use('/players', playersControllers);
app.use('/countries', countriesControllers);

module.exports = app;