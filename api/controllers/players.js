const express = require('express');
const router = express.Router();
const PlayerModel = require('../models/player');

router.get('/', (req, res) => {
    let response = PlayerModel.getAll();
    response.then((users) => {
        res.json(users);
    }, (err) => {
        res.json(err);
    });
});

module.exports = router;