'use strict';

const db = require('./db');
const Country = db.Country;

let CountryModel = {};

CountryModel.getAll = function () {
    console.log(" == CountryModel.getAll == ");
    return new Promise(((resolve, reject) => {
        Country.findAll({
            attributes: ["code", "name", "age_restriction"]
        }).then((countries) => {
            resolve(countries);
        }).catch(function (err) {
            reject(err);
        })
    }));
};

CountryModel.getPrioritized = function () {
    console.log(" == CountryModel.getAll == ");
    return new Promise(((resolve, reject) => {
        Country.findAll({
            where : {is_prioritized : 1},
            attributes: ["code", "name", "age_restriction"]
        }).then((countries) => {
            resolve(countries);
        }).catch(function (err) {
            reject(err);
        })
    }));
};


module.exports = CountryModel;