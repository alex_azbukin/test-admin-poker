'use strict';
module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    code: { type: DataTypes.STRING, allowNull: false },
    name: { type: DataTypes.STRING, allowNull: false },
    is_banned: { type: DataTypes.BOOLEAN, defaultValue: 0 },
    is_prioritized: { type: DataTypes.BOOLEAN, defaultValue: 0 },
    age_restriction: { type: DataTypes.INTEGER, defaultValue: 18 },
  }, {
    tableName: "country"
  });
  return Country;
};