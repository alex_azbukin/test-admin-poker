'use strict';

const bcrypt = require('bcrypt-nodejs');

module.exports = (sequelize, DataTypes) => {
  const Player = sequelize.define('Player', {
    first_name: { type: DataTypes.STRING, allowNull: false },
    second_name: { type: DataTypes.STRING, allowNull: true },
    email: { type: DataTypes.STRING, allowNull: false },
    nickname: { type: DataTypes.STRING, allowNull: false },
    password: { type: DataTypes.STRING, allowNull: false },
    gender: { type: DataTypes.STRING, allowNull: false, defaultValue: 'M' },
    phone_number: { type: DataTypes.STRING, allowNull: true },
    dob: { type: DataTypes.DATE, allowNull: false },
    country_id: { type: DataTypes.INTEGER, references: { model: 'Country', key: 'id' } },
    zip: { type: DataTypes.STRING, allowNull: true },
    city: { type: DataTypes.STRING, allowNull: true },
    address1: { type: DataTypes.STRING, allowNull: true },
    address2: { type: DataTypes.STRING, allowNull: true },
    is_address_verified: { type: DataTypes.BOOLEAN, defaultValue: false },
    avatar: { type: DataTypes.STRING, allowNull: true, defaultValue: 'https://banner2.kisspng.com/20180511/deq/kisspng-bart-simpson-the-simpsons-season-23-cartoon-avat-5af5df18d83058.7537275515260628728855.jpg' },
    referred_by: { type: DataTypes.STRING, allowNull: true },
    registration_code: { type: DataTypes.STRING, allowNull: true },
    registration_ip: { type: DataTypes.STRING, allowNull: true },
    licence_id: { type: DataTypes.INTEGER, allowNull: true },
    created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
  }, {
      indexes: [ { unique: true, fields: [ 'nickname', 'email', 'phone_number' ] } ],
      tableName: "player",
      underscored: true
  });

  Player.associate = function(models) {
      Player.belongsTo(models.Country, {as: 'Country'});
  };

  Player.beforeCreate(function (player, options) {
      if (!player.changed("password")) {
          return player;
      }

      let hash = bcrypt.hashSync(player.password);
      player.password = hash;
  });

  //Instance methods

  Player.prototype.checkPassword = function (checkPassword, done) {
      bcrypt.compare(checkPassword, this.password, function (err, isMatch) {
          done(err, isMatch);
      });
  };

  Player.prototype.name = function () {
    return this.first_name + " " + this.second_name;
  };

  return Player;
};