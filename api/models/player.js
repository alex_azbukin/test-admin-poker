'use strict';

const db = require('./db');
const Player = db.Player;

let PlayerModel = {};

PlayerModel.getAll = function () {
    console.log(" == PlayerModel.getAll == ");
    return new Promise(((resolve, reject) => {
        Player.findAll({
            include: [{model: db.Country, as: 'Country', attributes: ["name", "code"]}],
            attributes: ["nickname", "first_name", "second_name", "avatar", "created_at", "dob", "email", "phone_number", "city"]
        }).then((players) => {
            resolve(players);
        }).catch(function (err) {
            reject(err);
        })
    }));
};


module.exports = PlayerModel;