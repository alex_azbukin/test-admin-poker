const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const cookieSession = require('cookie-session');
const controllers = require('./controllers');

//enable config file {.env}
dotenv.config();

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cookieSession({
    name: 'mysession',
    keys: ['randomKey222*&^%$#@'],
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

//set up routes
app.use('/', controllers);

const PORT = process.env.SERVER_PORT || 4000;
const HOST = process.env.SERVER_HOST || 'localhost';

app.listen(PORT, function(){
    console.log('Server is running on Host:Port => ', HOST + ' : ' + PORT);
});