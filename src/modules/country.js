import axios from 'axios';

export default {
    getList() {
        return axios.get('/countries').then((response) => {
            return response.data;
        }).catch((error) => {
            console.log(error);
            return error;
        });
    },
    getPrioritized() {
        return axios.get('/countries/prioritized').then((response) => {
            return response.data;
        }).catch((error) => {
            console.log(error);
            return error;
        });
    }
}