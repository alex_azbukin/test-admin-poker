import axios from 'axios';

export default {
    getList() {
        return axios.get('/players').then((response) => {
            return response.data;
        }).catch((error) => {
            console.log(error);
            return error;
        });
    },
}