import VueRouter from 'vue-router';
import IndexPage from '@/pages/Index';
import PlayersPage from '@/pages/Players';
import PlayerCreatePage from '@/pages/PlayerCreate';
import CountriesPage from '@/pages/Countries';

const isLoggedIn = function (to, from, next) {
    next();
};

const routes = [
    {name: "index", path: "/", component: IndexPage, beforeEnter: isLoggedIn},
    {name: "players", path: "/players", component: PlayersPage, beforeEnter: isLoggedIn},
    {name: "player-create", path: "/players/new", component: PlayerCreatePage, beforeEnter: isLoggedIn},
    {name: "countries", path: "/countries", component: CountriesPage, beforeEnter: isLoggedIn},
];

const router = new VueRouter({mode: "history", routes: routes});

export default router;